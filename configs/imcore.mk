# ---------------------------------------------------------------
# Build PiBox Media Server UI
# ---------------------------------------------------------------

root-verify:
	@if [ $(UID) -ne 0 ]; then \
		$(MSG11) "You must be root." $(EMSG); \
		exit 1; \
	fi

opkg-verify:
	@if [ "$(OPKG_DIR)" = "" ] || [ ! -f $(OPKG_DIR)/opkg-build ]; then \
		$(MSG11) "Can't find opkg-build.  Try setting OPKG= to the directory it lives in." $(EMSG); \
		exit 1; \
	fi

# Retrieve package
.$(IMCORE_T)-get $(IMCORE_T)-get: 
	@touch .$(subst .,,$@)

.$(IMCORE_T)-get-patch $(IMCORE_T)-get-patch: .$(IMCORE_T)-get
	@touch .$(subst .,,$@)

# Unpack packages
.$(IMCORE_T)-unpack $(IMCORE_T)-unpack: .$(IMCORE_T)-get-patch
	@touch .$(subst .,,$@)

# Apply patches
.$(IMCORE_T)-patch $(IMCORE_T)-patch: .$(IMCORE_T)-unpack
	@touch .$(subst .,,$@)

.$(IMCORE_T)-init $(IMCORE_T)-init: .$(IMCORE_T)-patch
	@touch .$(subst .,,$@)

.$(IMCORE_T)-config $(IMCORE_T)-config: 

# Build the package
$(IMCORE_T): .$(IMCORE_T)

.$(IMCORE_T): .$(IMCORE_T)-init 
	@make --no-print-directory $(IMCORE_T)-config
	@touch .$(subst .,,$@)

$(IMCORE_T)-files:

# Package it as an opkg 
opkg pkg $(IMCORE_T)-pkg: .$(IMCORE_T)
	@make --no-print-directory root-verify opkg-verify
	@$(MSG) "================================================================"
	@$(MSG3) "Building opkg" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(PKGDIR)/opkg/imcore/CONTROL
	@cp -ar $(DIR_IMCORE)/config/* $(PKGDIR)/opkg/imcore/
	@cp $(DIR_IMCORE)/opkg/control $(PKGDIR)/opkg/imcore/CONTROL/control
	@cp $(DIR_IMCORE)/opkg/preinst $(PKGDIR)/opkg/imcore/CONTROL/preinst
	@cp $(DIR_IMCORE)/opkg/postinst $(PKGDIR)/opkg/imcore/CONTROL/postinst
	@cp $(DIR_IMCORE)/opkg/prerm $(PKGDIR)/opkg/imcore/CONTROL/prerm
	@cp $(DIR_IMCORE)/opkg/postrm $(PKGDIR)/opkg/imcore/CONTROL/postrm
	@cp $(DIR_IMCORE)/opkg/debian-binary $(PKGDIR)/opkg/imcore/CONTROL/debian-binary
	@sed -i 's%\[VERSION\]%'`cat $(TOPDIR)/version.txt`'%' $(PKGDIR)/opkg/imcore/CONTROL/control
	@chmod +x $(PKGDIR)/opkg/imcore/CONTROL/preinst
	@chmod +x $(PKGDIR)/opkg/imcore/CONTROL/postinst
	@chmod +x $(PKGDIR)/opkg/imcore/CONTROL/prerm
	@chmod +x $(PKGDIR)/opkg/imcore/CONTROL/postrm
	@chown -R root.root $(PKGDIR)/opkg/imcore/
	@cd $(PKGDIR)/opkg/ && $(OPKG_DIR)/opkg-build -O imcore
	@cp $(PKGDIR)/opkg/*.opk $(PKGDIR)/
	@echo rm -rf $(PKGDIR)/opkg

# Clean the packaging
pkg-clean $(IMCORE_T)-pkg-clean:
	@if [ "$(PKGDIR)" != "" ] && [ -d "$(PKGDIR)" ]; then rm -rf $(PKGDIR); fi

# Clean out a cross compiler build but not the CT-NG package build.
$(IMCORE_T)-clean: $(IMCORE_T)-pkg-clean
	@if [ "$(IMCORE_SRCDIR)" != "" ] && [ -d "$(IMCORE_SRCDIR)" ]; then rm -rf $(IMCORE_SRCDIR); fi
	@rm -f .$(IMCORE_T) .$(IMCORE_T)-get

# Clean out everything associated with IMCORE
$(IMCORE_T)-clobber: 
	@rm -rf $(BLDDIR) 
	@rm -f .$(IMCORE_T)-config .$(IMCORE_T)-init .$(IMCORE_T)-patch \
		.$(IMCORE_T)-unpack .$(IMCORE_T)-get .$(IMCORE_T)-get-patch
	@rm -f .$(IMCORE_T) 

