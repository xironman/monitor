## Synopsis

imcore is a package that modifies the PiBox Development Platform
to create the core platform for the IronMan Monitor.  It provides
an updated, consumer friendly boot platform on which the IronMan
project can be built.

## Installation

imcore is based on GNU Make.  It designed to build for the following 
hardware platforms:

    Raspberry Pi (PiBox Development Platform)

Before proceeding, read any Linux distribution-specific notes, such as 
"fedora.notes".  If there are none, then don't worry about it.

The build depends on the cdtools environment setup.  The environment
file is in scripts/pmsui.sh.  See cdtools documentation for it's use.
See cdtools:  http://www.graphics-muse.org/wiki/pmwiki.php/Cdtools/Cdtools

After editing ironman.sh, source the script:

    . cdtools

Then setup your environment by running the function in that script:

    ironman

Now you're ready to retrieve the source.  Run the following command to see
how to clone the source tree:

    cd?

This will tell you exactly what to do.  After you clone the source you can
do a complete build just by typing:

    sudo make pkg

## Contributors

To get involved with Ironman, contact the project administrator:
Michael J. Hammel <mjhammel@graphics-muse.org>

## License

0BSD
