#!/bin/sh
# Ironman network start/stop
#
# Start/stop the network....
#
WPACONF=/etc/wpa_supplicant.conf

NETWORK_DIR=/etc/network
INTERFACES=$NETWORK_DIR/interfaces
INTERFACES_APONLY=$NETWORK_DIR/interfaces.aponly
HOSTAPD=/etc/hostapd.conf
HOSTAPD_APONLY=$NETWORK_DIR/hostapd.aponly
DHCP=/etc/dhcpd.conf
DHCP_APONLY=/etc/network/dhcpd.conf.aponly
DHCP_UAP=/etc/network/dhcpd.conf.uap
IMGPIOPAIR=/etc/gpio/pair

# Shutdown wpa_supplicant
killSup()
{ 
    doWlan=`ps | grep wpa_supplicant | grep -v grep`
    if [ "$doWlan" != "" ]
    then
        killall wpa_supplicant                                             
    fi
}

# Shutdown hostapd
killAP()
{
    doWlan=`ps | grep hostapd | grep -v grep`
    if [ "$doWlan" != "" ]
    then
        killall hostapd                                             
    fi
    doWlan=`ps | grep dhcpd | grep -v grep`
    if [ "$doWlan" != "" ]
    then
        killall dhcpd                                             
    fi
    rm -rf /var/lib/dhcp
}

# Setup or teardown wireless or wired client.
# We assume that /etc/wpa_supplicant.conf has been properly configured.
client()
{
    case "$1" in
        start) 
            wpa_supplicant -B -c $WPACONF -Dnl80211 -iwlan0 &
            sleep 2
            ;;
    
        stop)
            killSup
            ;;
    esac
}

# Setup or teardown wireless access point
# We assume that /etc/hostapd.conf has been properly configured.
wap()
{
    case "$1" in
        start)
            # Create uap0 as a virtual interface on wlan0
            # We don't check if this is supported by the hardware.
            # For IronMan we assume it is.
            iw dev wlan0 interface add uap0 type __ap

            # Get a new MAC address for uap0 since it currently shares wlan0's.
            # Doing this to uap0 (sensor network) is less likely to collide than
            # on a home network.
            MACADDR="$(ip addr show wlan0 | awk '/ether/{print $2}')"
            SUFFIX=$(echo ${MACADDR} | cut -f6 -d":")
            NUM=$(printf "%d" 0x${SUFFIX})
            NUM=$(( ${NUM} + 1 ))
            NUM=$(( ${NUM} % 255 ))
            SUFFIX=$(printf "%02x" ${NUM})
            MACADDR="$(echo ${MACADDR} | cut -f1-5 -d":"):${SUFFIX}"
            ip link set uap0 address ${MACADDR}

            # Start dhcp on the new interface
            mkdir -p /var/lib/dhcp
            touch /var/lib/dhcp/dhcpd.leases
            ifdown wlan0
            ifup uap0
            dhcpd -cf $DHCP_UAP uap0
            hostapd -i uap0 -B $HOSTAPD
            ;;
    
        stop)
            killAP
            ;;
    esac
}

# Start a default AP.  We assume that the
# network device is available.
startDefaultAP()
{
    cp $INTERFACES_APONLY $INTERFACES

    mkdir -p /var/lib/dhcp
    touch /var/lib/dhcp/dhcpd.leases
    /sbin/ifup -a
    sleep 3
    hostapd -B $HOSTAPD_APONLY
    dhcpd -cf $DHCP_APONLY wlan0

    # Create sensor AP - we need this to allow 
    # configuration of the interface via libpnc.
    iw dev wlan0 interface add uap0 type __ap
}

# Now change the run state of the network.
case "$1" in
    start) 
        echo "Starting network..."
        /sbin/ifdown -a

        # Bring up loopback and wired interfaces immediately.
        /sbin/ifup lo
        /sbin/ifup eth0

        # Check if we're in PAIR mode
        if [[ -e ${IMGPIOPAIR} ]]
        then
            PAIR_MODE=1
        else
            PAIR_MODE=0
        fi

        if [ ${PAIR_MODE} -eq 1 ]
        then
            # We're in PAIR mode.
            # The user is trying to configure the AP and 
            # internet connection via a mobile device.

            # Start AP in default configuration.
            startDefaultAP

            # Resetting from this will happen when the web
            # server reboots the device.
        else
            # Not in PAIR mode.

            # Start internet connection via WiFi
            client $1

            # Bring up client interface temporarily.
            /sbin/ifup wlan0

            # Start AP for IoT devices, which will down wlan0
            wap $1

            # Bring up client interface again.
            /sbin/ifup wlan0

            # Use these to push the wlan throughput.
            # The txpower update can only be done if you change the region.
            # /sbin/iwconfig wlan0 down
            # /usr/sbin/iw reg set BO
            # /sbin/iwconfig wlan0 up
            # /sbin/iwconfig wlan0 txpower 30
            /sbin/ifconfig wlan0 txqueuelen 10000
        fi
        ;;

    stop)
        echo "Stopping network..."
        /sbin/ifdown -a
        wap $1
        client $1
        ;;

    restart|reload)
        "$0" stop
        "$0" start
        ;;

    *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1
esac
exit $?

